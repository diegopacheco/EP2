package ep2;

public final class Game {
    
    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;
    private static final int DELAY = 10;
    
    public static int getWidth(){ // Retorna o valor de largura
        return WIDTH;
    }
    
    public static int getHeight(){ // Retorna o valor de altura
        return HEIGHT;
    }
    
    public static int getDelay(){ //Retorna o valor do deley
        return DELAY;
    }
}
