package ep2;

public class Explosion extends Sprite {
	
	public Explosion (int x, int y) {
		super(x,y);
		initExplosion();
	}
	
	public void initExplosion() { // Pega a imagem e sua dimensão
		loadImage("images/explosion.png");
		getImageDimensions();
	}
	public void explosionShip() {
		loadImage("images/explosion_ship.png");
	}
	
}
