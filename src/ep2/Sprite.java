	package ep2;

import java.awt.Rectangle;		
import java.awt.Image;
import javax.swing.ImageIcon;

public abstract class Sprite {

    protected int x;
    protected int y;
    protected int width;
    protected int height;
    protected boolean vis;
    protected Image image;

    public Sprite(int x, int y) {

        this.x = x;
        this.y = y;
        vis = true;
    }

	protected void loadImage(String imageName) { //Cria o icone da imagem 

        ImageIcon ii = new ImageIcon(imageName);
        image = ii.getImage();
    }
    
    protected void getImageDimensions() { //Pega as dimensões x e y da imagem

        width = image.getWidth(null);
        height = image.getHeight(null);
    }    

    public Image getImage() { //Retorna a imagem
        return image;
    }

    public int getX() { // Retorna o valor de x
        return x;
    }

    public int getY() { // Retorna o valor de y
        return y;
    }
    
    public int getWidth(){ // Retorna o valor de largura
        return width;
    }
    
    public int getHeight(){ // Retorna o valor de altura
        return height;
    }

    public boolean isVisible() { // Retorna o valor de vis
        return vis;
    }

    public void setVisible(Boolean visible) { // Diz que o valor de vis é visible
        vis = visible; 
    }
    
    public Rectangle getBounds() { // Cria o retangulo de acordo com a posição do sprite
        return new Rectangle(x, y, width, height);
    }
}