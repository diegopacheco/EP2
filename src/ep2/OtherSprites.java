package ep2;

public class OtherSprites extends Sprite {
	
	
	public OtherSprites(int x, int y, int LIFE) {
		super(x, y);
		switch (LIFE) {
		case 0:
			number_0();
			break;
		case 1:
			number_1();
			break;

		case 2:
			number_2();
			break;

		case 3:
			number_3();
			break;

		case 4:
			number_4();
			break;

		case 5:
			number_5();
			break;

		case 6:
			number_6();
			break;

		case 7:
			number_7();
			break;
		case 8:
			number_8();
			break;	
		case 9:
			number_9();
			break;
		default:
			break;
		}
		
	}
	
	public void initBonus() {
		loadImage("images/bonus.png");
		getImageDimensions();
	}
	
	public void pill_life() {
		loadImage("images/pill.png");
		getImageDimensions(); 
	}
	
	public void move() {	//Faz o objeto mover mover para baixo
		y += 1;
	}
	
	public void spaceship_life() {
		loadImage("images/spaceship_life.png");
	}
	
	public void letter_x() {
		loadImage("images/letter_x.png");
	}
	
	public void number_0() {
		loadImage("images/number_0.png");
	}
	
	public void number_1() {
		loadImage("images/number_1.png");
	}
	public void number_2() {
		loadImage("images/number_2.png");
	}
	public void number_3() {
		loadImage("images/number_3.png");
	}
	public void number_4() {
		loadImage("images/number_4.png");
	}
	public void number_5() {
		loadImage("images/number_5.png");
	}
	public void number_6() {
		loadImage("images/number_6.png");
	}
	public void number_7() {
		loadImage("images/number_7.png");
	}
	public void number_8() {
		loadImage("images/number_8.png");
	}
	public void number_9() {
		loadImage("images/number_9.png");
	}
	
}
