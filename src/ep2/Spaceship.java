package ep2;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class Spaceship extends Sprite {
    
	private static final int MAX_SPEED_X = 2;
    private static final int MAX_SPEED_Y = 1;
    private ArrayList<Missile> missiles;
   
    private int speed_x;
    private int speed_y;

    public Spaceship(int x, int y) {
        super(x, y);
        
        initSpaceShip();
    }
    
    private void initSpaceShip() { //Chama a função noThrust e cria um Array de missel
        
        noThrust();
        missiles = new ArrayList<>();
        
    }
    
    private void noThrust(){ //Pega a imagem e suas dimensões

    	
    	loadImage("images/spaceship.png");
        getImageDimensions(); 
    }
    
    public void move() { //Move a a nave e verifica se chegou nas bordas
        
    	// Limits the movement of the spaceship to the side edges.
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
        // Moves the spaceship on the horizontal axis
        x += speed_x;
        
        // Limits the movement of the spaceship to the vertical edges.
        if((speed_y < 0 && y <= 0) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        // Moves the spaceship on the verical axis
        y += speed_y;

    }
    
    public ArrayList getMissiles() { //Retorna a o array do missel
    	return missiles;
    }

    public void keyPressed(KeyEvent e) { //Recebe a ação do teclado e de acordo com ela, irá somar a posição da nave

        int key = e.getKeyCode();
        
        if (key == KeyEvent.VK_SPACE) {
			fire();
		}
        // Set speed to move to the left
        if (key == KeyEvent.VK_LEFT) { 
            speed_x = -1 * MAX_SPEED_X;
          
        }

        // Set speed to move to the right
        if (key == KeyEvent.VK_RIGHT) {
            speed_x = MAX_SPEED_X;
           ;
        }
        
        // Set speed to move to up and set thrust effect
        if (key == KeyEvent.VK_UP) {
            speed_y = -1 * MAX_SPEED_Y;
        }
        
        // Set speed to move to down
        if (key == KeyEvent.VK_DOWN) {
            speed_y = MAX_SPEED_Y;
        }
        
    }
    
    public void fire() { // Cria um missel e adiciona a sua posição de acordo com a posição da nave
    	missiles.add(new Missile(x + 17 , y-12 ));
    }
    
    public void keyReleased(KeyEvent e) { 

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
           
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
            noThrust();
        }
    }
}