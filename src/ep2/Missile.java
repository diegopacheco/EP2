package ep2;

public class Missile extends Sprite {
	
	private static final int BOARD_WIDTH = 0;
	private static final int MAX_SPEED_Y = 3;
	
	
	public Missile(int x, int y) {
		super(x,y);
		initMissile();
	}
	
	public void initMissile() { //Pega a imagem e sua posição
		loadImage("images/missile.png");
		getImageDimensions();
	}
	
	public void move(){ //Move o missel, subitraindo a posição de Y
		
		y -= MAX_SPEED_Y;
		if (y == BOARD_WIDTH) {
			vis = false;
		}
	}	
	
	
}
