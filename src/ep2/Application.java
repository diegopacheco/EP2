package ep2;


import javax.swing.JFrame;

import java.awt.EventQueue;


@SuppressWarnings("serial")
public class Application extends JFrame{

	
	
	public Application() {
		initUI();
		
	}
	
	private void initUI() { //Cria o mapa no swing
		
		
		add(new Map());
		setResizable(false);
		pack();
		setSize(Game.getWidth(), Game.getHeight());

        setTitle("Space X");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
	}
	
	public static void main(String[] args) { // Executa o swing, fazendo o jogo rodar
		
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable(){
			@Override
			public void run() {
			
				Application ex = new Application();
				ex.setVisible(true);
										
			}
		}); 
		
		
		
	}

}