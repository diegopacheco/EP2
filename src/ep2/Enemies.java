package ep2;



public class Enemies extends Sprite {
	
		
	public Enemies(int x, int y, int LEVEL) { //Irá chamar a criação do inimigo de acordo com os pontos
		super(x,y);
	
		if ( LEVEL < 500 ) {
			initEnemieShipEasy();
		}
		else if (LEVEL >= 500 && LEVEL <= 1500){
			initEnemieShipMedium();
		}
		else if (LEVEL > 1500) {
			initEnemieShipHard();
		}
		
	}
	
	public void initEnemieShipEasy() { //Pega a imagem e sua posição
		loadImage("images/alien_EASY.png");
		getImageDimensions();
	}
	public void initEnemieShipMedium() {
		loadImage("images/alien_MEDIUM.png");
		getImageDimensions();
	}
	public void initEnemieShipHard() {
		loadImage("images/alien_HARD.png");
		getImageDimensions();
	}
	public void move() {	//Faz o inimigo mover para baixo
		y += 1;
	}

	/*
	@Override
	public synchronized void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("TEse");

			
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    } */
	
}
