package ep2;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Map extends JPanel implements ActionListener {
	
	private ArrayList <Enemies> enemies = new ArrayList<>();
	private boolean ingame;
	private boolean wingame;
	private boolean ini;
	private final Random gerador = new Random();
	private final Random gerador_2 = new Random();
    private final int SPACESHIP_X = 220;
    private final int SPACESHIP_Y = 430;
    private int life = 2;
    private int cont = 0;	
    private int cont_pontos = 0;
    private int cont_life = 0;
    private int cont_shield = 0;
    private int ENEMIE_X = 0;
    private int ENEMIE_Y = 0;
    private Timer timer_map;
    private int pontos = 0;
    
    private OtherSprites bonus, pill, spaceship_life, number, letter, score_1, score_2, score_3, score_4, score_5;
    private Image background;
    private Spaceship spaceship;
    private Explosion explosion;
    private JButton iniciar,sair,restart;
    
    public Map(){
    	
    	initMap();
                  
    }
    
    private  void initMap() { //Cria os botões / Cria a explosão e naves  e o plano de fundo

    	iniciar = new JButton("INICIAR");
    	iniciar.setBackground(new Color(79,79,79));	
        add(iniciar);
        iniciar.addActionListener( this );
        
        sair = new JButton("SAIR");
        sair.setBackground(new Color(79, 79, 79));
        add(sair);
        sair.addActionListener(this);
        
        restart = new JButton("CONTINUAR");
        restart.setBackground(new Color(79, 79, 79));
        add(restart);
        restart.addActionListener( this );
        restart.setVisible(false);
                
    	explosion = new Explosion(0,0);
    	explosion.setVisible(false);
    	
    	addKeyListener(new TAdapter());
        setFocusable(true);
        //setBackground(Color.BLACK);
        ImageIcon image = new ImageIcon("images/background.jpg");
        this.background = image.getImage();
        
        //enemies.clear();
        ini = true;
        ingame = false;
        wingame = false;
        
        letter = new OtherSprites(420, 10, 5);
        number = new OtherSprites(450, 10, 5);
        
        pill = new OtherSprites(0, 0, 0);
        pill.setVisible(false);
        
        bonus = new OtherSprites(0, 0, 0);
        bonus.setVisible(false);
               
        spaceship_life = new OtherSprites(400,10,0);
        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
                        
        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
    }
    
    
    public void initEnemies() { // Faz a criação dos inimigos adicionando no Array
    			
    			int posX = (int)(gerador.nextInt(470)+210);
    			int posY = 0;
    			enemies.add(new Enemies(posX,posY,cont_pontos));
    			
    			int pos_X = (int)(gerador.nextInt(210));
    			int pos_Y = 0;
    			enemies.add(new Enemies(pos_X,pos_Y,cont_pontos));
    	
    }
    
        
    @Override
    public void paintComponent(Graphics g) { //Chama as classes para criação das imagens
        super.paintComponent(g);
        if(ini) {
        	g.drawImage(this.background, 0, 0, null);
        	dranMenu(g);
        }
        
        if(ingame) {
        	
        	g.drawImage(this.background, 0, 0, null);
        	drawObjects(g);
        }
        else if(wingame){

			g.drawImage(this.background, 0, 0, null);
			dranMissionAccomplished(g);
			
		} 
        else if(!ingame && !ini){
			g.drawImage(this.background, 0, 0, null);
			drawGameOver(g);
		}
        
    

        Toolkit.getDefaultToolkit().sync();
    }
    
    private void Score(Graphics g) { // Cria a imagem dos Scores
    	score_5 = new OtherSprites(5, 10, 0);
    	g.drawImage(score_5.getImage(), score_5.getX(), score_5.getY(), this);
    	

    	score_4 = new OtherSprites(15, 10, cont_pontos/1000);
    	g.drawImage(score_4.getImage(), score_4.getX(), score_4.getY(), this);
    	

    	score_3 = new OtherSprites(25, 10, (cont_pontos%1000)/100);
    	g.drawImage(score_3.getImage(), score_3.getX(), score_3.getY(), this);
    	

    	score_2 = new OtherSprites(35, 10, (cont_pontos%100)/10);
    	g.drawImage(score_2.getImage(), score_2.getX(), score_2.getY(), this);
    	

    	score_1 = new OtherSprites(45, 10, 0);
    	g.drawImage(score_1.getImage(), score_1.getX(), score_1.getY(), this);
    	
    }
    
    private void drawObjects(Graphics g) { //Cria a imagens das sprites
    	
    	Score(g);
    	
    	spaceship_life.spaceship_life();
    	g.drawImage(spaceship_life.getImage(),spaceship_life.getX(),spaceship_life.getY(), this);
    	
    	letter.letter_x();
    	g.drawImage(letter.getImage(), letter.getX(), letter.getY(), this);
    	
    	number = new OtherSprites(440, 10, life);
    	g.drawImage(number.getImage(), number.getX(), number.getY(), this);
    	
    	if (pill.isVisible()) {
    		pill.pill_life();
			g.drawImage(pill.getImage(), pill.getX(), pill.getY(), this);
		}
    	
    	if (bonus.isVisible()) {
			bonus.initBonus();
			g.drawImage(bonus.getImage(), bonus.getX(), bonus.getY(), this);
		}
    	
    	if (spaceship.isVisible()) {
    		g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
		}
    	ArrayList<Missile> ms = spaceship.getMissiles();
    	    	
    	for (Missile m : ms) {
			if (m.isVisible()) {
				g.drawImage(m.getImage(), m.getX(), m.getY(), this);
			}
		}    	
    	for (Enemies a : enemies) {

    			if (a.isVisible()) {
    				g.drawImage(a.getImage(), a.getX(), a.getY(),this);
    			}
    	}
    	if (explosion.isVisible()) {
        	g.drawImage(explosion.getImage(), ENEMIE_X, ENEMIE_Y, this);
		}
    	g.setColor(Color.white);
    	//g.drawString("Pontos: " + cont_pontos, 5,15);
    } 
    
    private void drawGameOver(Graphics g) { //Faz  a criação da imagem  quando o jogo acaba

        String message = "SCORE: " + cont_pontos;
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics metric = getFontMetrics(font);
        
        restart.setVisible(true);
        sair.setVisible(true);
            
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
                
    }
        
    private void dranMissionAccomplished(Graphics g) { // Faz a criação da imagem quando ganha
    	
        String message = "MISSION ACCOMPLISHED";
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics metric = getFontMetrics(font);
        
        restart.setVisible(true);
        sair.setVisible(true);
        
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
	
    
    private void dranMenu(Graphics g) { // Faz a criação da imagem do menu inicial
    	
        String message = "SPACE X";
        Font font = new Font("Helvetica", Font.BOLD, 50);
        FontMetrics metric = getFontMetrics(font);
        
        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
        
        
    }
    
    private void button(ActionEvent e) { //Faz as ações dos botões
    	if (e.getSource() == iniciar) {
    		iniciar.setVisible(false);
    		iniciar.remove(iniciar);
    		sair.setVisible(false);
    		ini = false;
    		ingame = true;
    	}
    	else if (e.getSource() == sair) {
			System.exit(0);
		}
    	else if(e.getSource() == restart) {
    		
    		enemies.removeAll(enemies);
    		sair.setVisible(false);
    		restart.setVisible(false);
    		pontos = 0;
    		cont_pontos = 0;
    		cont = 0;
    		timer_map.start();
    		spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
    		explosion.setVisible(false);
    		life = 2; 
    		initEnemies();
    		ingame = true;
    		
    		
    	}	
    }
    
    @Override
    public void actionPerformed(ActionEvent e) { // Função que irá iniciar e atualizar o swing
 
    	  	
	 	if(ingame){
    		inGame();
        	WinPontos();
            updateSpaceship();
            updateLife();
            updateMissile();
            updateEnemies();
            updateBonus();
            bonus();
            
            checkCollisions();
            Ver_Pontos();
            repaint();
    	}
	 	else {
	 		button(e);
		}
    }
    
    
    private void Ver_Pontos() { //Irá chamar a criação dos inimigos de acordo com os updates do swing, criando os sistemas de fazes, onde irá diminuir o intervalo de criação
    	if(ingame) {
	    	cont++;
	    	if(cont_pontos <= 500) {
	        	if (cont % 40 == 0) {
					cont = 0;
					initEnemies();
				}
	        }
	        else if(cont_pontos > 500 && cont_pontos <= 1500) {
	        	if (cont % 30 == 0) {
					cont = 0;
					initEnemies();
				}
	        }
	        else if (cont_pontos > 1500 ) {
	        	if (cont % 20 == 0) {
					cont = 0;
					initEnemies();
				}
	        }
	    	
	    	if (cont == 5) {
	            explosion.setVisible(false);
			}
    	}
    }
    
    private void bonus() { //Verifica se passaram 50 pontos para criar ponto de vida
    	if (cont_life == 50) {
      		pill = new OtherSprites(gerador_2.nextInt(470), -10, 0);
            pill.setVisible(true);
            cont_life = 0;
		}
    	if (cont_shield == 30) {
			bonus = new OtherSprites(gerador_2.nextInt(470), -10, 0);
			bonus.setVisible(true);
			cont_shield = 0;
		}
    }	
    
    private void inGame() { // Para o jogo se ingame for falso, ou seja se perder o jogo
    	if (!ingame && !ini) {
			timer_map.stop();
		}
    } 
    
    private void updateLife() {
    	if (life < 0) {
			ingame = false;
		}
    }
     
    private void updateBonus() {
    	if (pill.isVisible()) {
    		pill.move();
		}
    	if (bonus.isVisible()) {
			bonus.move();
		}
    }
    
    private void updateSpaceship() { //Faz a atualização da nave
    	if(spaceship.isVisible()) {
            spaceship.move();	
    	}
    	else {
    		ENEMIE_X = spaceship.getX();
    		ENEMIE_Y = spaceship.getY();
    		explosion.setVisible(true);
    		try {
				Thread.sleep(1000, 2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("nao deu");
			}
    	}
    }
    
    private void updateMissile() { //Faz a atualização dos misseis, removendo se não for visevel 
    	ArrayList<Missile> ms = spaceship.getMissiles();
    	
    	for (int i = 0; i < ms.size(); i++) {
    		
			Missile m = ms.get(i);
			
			if (m.isVisible()) {
				m.move();
			}
			else {
				ms.remove(i);
			}
			if (m.y == Game.getDelay()) {
				ms.remove(i);
			}
		}	
    }
    
    private void WinPontos() { //Irá terminar o jogo se atingir 3000 de pontos
        if(cont_pontos >= 3500) {
    		ingame = false;
			wingame = true;
			return;
		}
    }
        
    private void  updateEnemies() { //Atualiza o inimigo, removendo se não for visivel
    	for (int i = 0; i < enemies.size(); i++) {
			Enemies a = enemies.get(i);
			if (a.isVisible()) {
				a.move();
				if (a.y == Game.getHeight()) {
					enemies.remove(i);
				}
				
			}else {
				enemies.remove(i);
			}	
		}
    }
    
      
    public void checkCollisions() { //Faz a checagem de colisão entre inimigo e misseis e inimigo e espaçonave
    	
    	
    	Rectangle r3 = spaceship.getBounds();
    	for (Enemies enemie : enemies) {    		
			Rectangle r2 = enemie.getBounds();
			
			if (r3.intersects(r2)) {
				enemie.setVisible(false);
				ENEMIE_X = enemie.getX();
    			ENEMIE_Y = enemie.getY();
    			explosion.explosionShip();
    			explosion.setVisible(true);
				life -= 1;
				
			}
		}
    	Rectangle r4 = pill.getBounds();
    	if (r3.intersects(r4)) {
    		pill.setVisible(false);
    		if (life < 2) {
				life += 1;
			}
		}
    	
    	Rectangle r5 = bonus.getBounds();
    	if (r3.intersects(r5)) {
    		bonus.setVisible(false);
    		cont_pontos += 2;
		}
    	
    	ArrayList<Missile> ms = spaceship.getMissiles();
 	
    	for (Missile m : ms) {
			
    		Rectangle r1 = m.getBounds();
    		
    		for (int i = 0; i<enemies.size(); i++) {
    			
    			Rectangle r2 = enemies.get(i).getBounds();
        		
        		if (r1.intersects(r2)) {
        			ENEMIE_X = enemies.get(i).getX();
        			ENEMIE_Y = enemies.get(i).getY();
    				m.setVisible(false);
    				enemies.remove(i);
    				explosion.initExplosion();
    				explosion.setVisible(true);
    				cont_pontos += 10;
    				cont_life++;
    				cont_shield++;
    			}
			}
		}
    }
  

    private class TAdapter extends KeyAdapter { // Envia a ação do teclado para classe missel
        
    	
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
                       
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);

			//initEnemies();
        }

    }
} 